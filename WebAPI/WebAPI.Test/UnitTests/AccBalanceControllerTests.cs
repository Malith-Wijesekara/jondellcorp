using Microsoft.AspNetCore.Mvc;
using System;
using WebAPI.Controllers;
using Xunit;
using Moq;
using WebAPI.Data;
using System.Threading.Tasks;
using WebAPI.Models;
using AutoMapper;
using WebAPI.Dtos;
using System.Collections.Generic;
using Dtos;
using System.Linq;

namespace WebAPI.UnitTests
{
    public class AccBalanceControllerTests
    {
        #region snippet_GetAccBalanceReturnsOk_WithABalanceRecord
        [Fact]
        public async Task GetAccBalanceReturnsOk_WithABalanceRecord()
        {
            // Arrange
            int yearToGetBalance = 2019;
            int monthToGetBalance = 3;
            var mockRepo = new Mock<IAccBalanceRepository>();
            mockRepo.Setup(repo => repo.GetAccBalance(yearToGetBalance, monthToGetBalance))
                .ReturnsAsync(new AccountBalance
                {
                    Year = 2019,
                    Month = 3,
                    RAndD = 100000,
                    Canteen = 150000,
                    CeoCar = -20000,
                    Marketing = -15000,
                    ParkingFines = 30000
                });

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(x => x.Map<AccBalanceForCreateDto>(It.IsAny<AccountBalance>())).Returns(new AccBalanceForCreateDto
            {
                Year = 2019,
                Month = 3,
                RAndD = 100000,
                Canteen = 150000,
                CeoCar = -20000,
                Marketing = -15000,
                ParkingFines = 30000
            });
            var controller = new AccBalanceController(mockRepo.Object, mockMapper.Object);

            // Act
            var result = await controller.GetAccBalance(yearToGetBalance, monthToGetBalance);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);

            var okObjectResult = result as OkObjectResult;
            var model = okObjectResult.Value as AccBalanceForCreateDto;

            Assert.NotNull(model);
            Assert.Equal(yearToGetBalance, model.Year);
            Assert.Equal(monthToGetBalance, model.Month);
        }
        #endregion

        #region snippet_GetMonthListReturnsOk_WithAMonthList
        [Fact]
        public async Task AddBalanceSheetReturnsCreatedStatusCode_WhenNewRecordIsCreated()
        {
            // Arrange
            var accBalanceForCreateDto = new AccBalanceForCreateDto
            {
                Year = 2019,
                Month = 3,
                RAndD = 100000,
                Canteen = 150000,
                CeoCar = -20000,
                Marketing = -15000,
                ParkingFines = 30000
            };

            var mockRepo = new Mock<IAccBalanceRepository>();
            mockRepo.Setup(repo => repo.GetAccBalance(accBalanceForCreateDto.Year, accBalanceForCreateDto.Month))
                .ReturnsAsync((AccountBalance)null);

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(x => x.Map<AccBalanceForCreateDto>(It.IsAny<AccountBalance>())).Returns((AccBalanceForCreateDto)null);

            var controller = new AccBalanceController(mockRepo.Object, mockMapper.Object);

            // Act
            var result = await controller.AddBalanceSheet(accBalanceForCreateDto);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<StatusCodeResult>(result);

            var statusCodeResult = result as StatusCodeResult;

            Assert.NotNull(statusCodeResult);
            Assert.Equal(statusCodeResult.StatusCode.ToString(), "201");
        }
        #endregion

        #region snippet_AddBalanceSheetReturnsOk_WhenARecordIsUpdated
        [Fact]
        public async Task AddBalanceSheetReturnsOk_WhenARecordIsUpdated()
        {
            // Arrange
            var accBalanceForCreateDto = new AccBalanceForCreateDto
            {
                Year = 2019,
                Month = 3,
                RAndD = 100000,
                Canteen = 150000,
                CeoCar = -20000,
                Marketing = -15000,
                ParkingFines = 30000
            };

            var mockRepo = new Mock<IAccBalanceRepository>();
            mockRepo.Setup(repo => repo.GetAccBalance(accBalanceForCreateDto.Year, accBalanceForCreateDto.Month))
                .ReturnsAsync(new AccountBalance
            {
                Year = 2019,
                Month = 3,
                RAndD = 20000,
                Canteen = 300000,
                CeoCar = -8000,
                Marketing = -3000,
                ParkingFines = 30000
            });

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(x => x.Map<AccBalanceForCreateDto>(It.IsAny<AccountBalance>())).Returns((AccBalanceForCreateDto)null);

            var controller = new AccBalanceController(mockRepo.Object, mockMapper.Object);

            // Act
            var result = await controller.AddBalanceSheet(accBalanceForCreateDto);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<StatusCodeResult>(result);
        }
        #endregion

        #region snippet_GetAccBalanceReturnsBadRequest_WhenRecoredNotExists
        [Fact]
        public async Task GetAccBalanceReturnsBadRequest_WhenRecoredNotExists()
        {
            // Arrange
            int yearToGetBalance = 2010;
            int monthToGetBalance = 3;
            var mockRepo = new Mock<IAccBalanceRepository>();
            mockRepo.Setup(repo => repo.GetAccBalance(yearToGetBalance, monthToGetBalance))
                .ReturnsAsync((AccountBalance)null);

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(x => x.Map<AccBalanceForCreateDto>(It.IsAny<AccountBalance>())).Returns((AccBalanceForCreateDto)null);
            var controller = new AccBalanceController(mockRepo.Object, mockMapper.Object);

            // Act
            var result = await controller.GetAccBalance(yearToGetBalance, monthToGetBalance);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);
        }
        #endregion

        #region snippet_GetYearListReturnsOk_WithAYearList
        [Fact]
        public async Task GetYearListReturnsOk_WithAYearList()
        {
            // Arrange
            List<int> yearList = new List<int>();
            yearList.Add(2018);
            yearList.Add(2019);
            yearList.Add(2020);

            var mockRepo = new Mock<IAccBalanceRepository>();
            mockRepo.Setup(repo => repo.GetYearList())
                .ReturnsAsync(yearList);

            var mockMapper = new Mock<IMapper>();
            var controller = new AccBalanceController(mockRepo.Object, mockMapper.Object);

            // Act
            var result = await controller.GetYearList();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);

            var okObjectResult = result as OkObjectResult;
            var model = okObjectResult.Value;
            Assert.NotNull(model);
            Assert.Equal(yearList, model);
        }
        #endregion

        #region snippet_GetMonthListReturnsOk_WithAMonthList
        [Fact]
        public async Task GetMonthListReturnsOk_WithAMonthList()
        {
            // Arrange
            var monthList = new List<int>();
            monthList.Add(2);
            monthList.Add(4);
            monthList.Add(3);
            monthList.Add(6);

            var yearToGetMonths = 2018;

            var mockRepo = new Mock<IAccBalanceRepository>();
            mockRepo.Setup(repo => repo.GetMonthList(yearToGetMonths))
                .ReturnsAsync(monthList);

            var mockMapper = new Mock<IMapper>();
            var controller = new AccBalanceController(mockRepo.Object, mockMapper.Object);

            // Act
            var result = await controller.GetMonthList(yearToGetMonths);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);

            var okObjectResult = result as OkObjectResult;
            var model = okObjectResult.Value;
            Assert.NotNull(model);
            Assert.Equal(monthList, model);
        }
        #endregion

        #region snippet_GetReportReturnsOk_WithABalanceRecordList
        [Fact]
        public async Task GetReportReturnsOk_WithABalanceRecordList()
        {
            // Arrange
            var periodForReportDto = new PeriodForReportDto
            {
                StartYear = 2019,
                StartMonth = 3,
                EndYear = 2019,
                EndMonth = 6
            };

            var mockData = new List<AccountBalance>();
            mockData.Add(new AccountBalance { Year = 2019, Month = 3, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });
            mockData.Add(new AccountBalance { Year = 2019, Month = 4, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });
            mockData.Add(new AccountBalance { Year = 2019, Month = 5, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });
            mockData.Add(new AccountBalance { Year = 2019, Month = 6, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });

            var reportForReturnDto = new List<ReportForReturnDto>();
            reportForReturnDto.Add(new ReportForReturnDto { Year = 2019, Month = 3, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });
            reportForReturnDto.Add(new ReportForReturnDto { Year = 2019, Month = 4, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });
            reportForReturnDto.Add(new ReportForReturnDto { Year = 2019, Month = 5, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });
            reportForReturnDto.Add(new ReportForReturnDto { Year = 2019, Month = 6, RAndD = 100000, Canteen = 150000, CeoCar = -20000, Marketing = -15000, ParkingFines = 30000 });

            var mockRepo = new Mock<IAccBalanceRepository>();
            mockRepo.Setup(repo => repo.GetReport(periodForReportDto))
                .ReturnsAsync(mockData);

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(x => x.Map<List<ReportForReturnDto>>(It.IsAny<List<AccountBalance>>())).Returns(reportForReturnDto);

            var controller = new AccBalanceController(mockRepo.Object, mockMapper.Object);

            // Act
            var result = await controller.GetReport(periodForReportDto);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);

            var okObjectResult = result as OkObjectResult;
            var model = okObjectResult.Value as List<ReportForReturnDto>;

            Assert.NotNull(model);

            Assert.Equal(periodForReportDto.StartYear.ToString(), model.First().Year.ToString());
            Assert.Equal(periodForReportDto.StartMonth.ToString(), model.First().Month.ToString());

            Assert.Equal(periodForReportDto.EndYear.ToString(), model.Last().Year.ToString());
            Assert.Equal(periodForReportDto.EndMonth.ToString(), model.Last().Month.ToString());
        }
        #endregion



    }
}