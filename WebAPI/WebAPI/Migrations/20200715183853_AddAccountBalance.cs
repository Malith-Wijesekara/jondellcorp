﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class AddAccountBalance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountBalance",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    RAndD = table.Column<double>(nullable: true),
                    Canteen = table.Column<double>(nullable: true),
                    CeoCar = table.Column<double>(nullable: true),
                    Marketing = table.Column<double>(nullable: true),
                    ParkingFines = table.Column<double>(nullable: true)                    
                    
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountBalance", x => x.Id);
                    // table.PrimaryKey(
                    //     name: "PK_AccountBalance",
                    //     columns: t => new {t.Year, t.Month});                    
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountBalance");
        }
    }
}
