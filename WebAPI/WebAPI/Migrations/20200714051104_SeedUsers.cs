﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
namespace WebAPI.Migrations
{
    public partial class SeedUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [UserName], [NormalizedUsername]) VALUES (N'59693484-a999-4f35-83de-be4bb6ce3b38', NULL, 0, N'AQAAAAEAACcQAAAAEBzlX3B0tFpUz+jY11L60MjCdm1pQdy553inOukZI6TS75A8wN00/EsA/IMrFz2eDw==', N'82e105a6-3cb2-4a19-8e6f-133366941046', NULL, 0, 0, NULL, 0, 0, 'ApplicationUser', N'Test_Admin', N'TEST_ADMIN')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [UserName], [NormalizedUsername]) VALUES (N'b2968337-e2ea-4e16-a684-943c14d093bc', NULL, 0, N'AQAAAAEAACcQAAAAEM6Eubf2kN9fLDqAnfxdK26b41cY1d+whubOixlG8p+GKB+S2IQorLpuSKOvRKp/Vw==', N'11fd1116-996f-46cd-a137-e45585e8445b', NULL, 0, 0, NULL, 0, 0, 'ApplicationUser', N'Test_User', N'TEST_USER')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName]) VALUES (N'39b0a061-c2dd-49c8-bc5a-8290a8d8fc24', N'User', N'User')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName]) VALUES (N'dda73520-5b6f-4258-a33d-fe244a6e129b', N'Admin', N'ADMIN')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'59693484-a999-4f35-83de-be4bb6ce3b38', N'dda73520-5b6f-4258-a33d-fe244a6e129b')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b2968337-e2ea-4e16-a684-943c14d093bc', N'39b0a061-c2dd-49c8-bc5a-8290a8d8fc24')
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
