using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data;
using WebAPI.Dtos;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccBalanceController : ControllerBase
    {
        private readonly IAccBalanceRepository _repo;
        private readonly IMapper _mapper;

        public AccBalanceController(IAccBalanceRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;

        }

        [Authorize(Roles = "Admin, User")]
        [HttpGet("{Year}/{Month}")]
        public async Task<IActionResult> GetAccBalance(int Year, int Month)
        {

            var accBalance = await _repo.GetAccBalance(Year, Month);

            var balanceToReturn = _mapper.Map<AccBalanceForCreateDto>(accBalance);

            if (balanceToReturn != null)
            {
                return Ok(balanceToReturn);
            }

            return BadRequest("File not exists");

        }

        [Authorize(Roles = "Admin,User")]
        [HttpGet("yearlist")]
        public async Task<IActionResult> GetYearList()
        {
            var yearlist = await _repo.GetYearList();

            if (yearlist != null)
            {
                return Ok(yearlist);
            }

            return BadRequest("Failed to retrive the year list");
        }

        [Authorize(Roles = "Admin,User")]
        [HttpGet("monthlist/{Year}")]
        public async Task<IActionResult> GetMonthList(int Year)
        {
            var monthlist = await _repo.GetMonthList(Year);

            if (monthlist != null)
            {
                return Ok(monthlist);
            }

            return BadRequest("Failed to retrive the month list");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("exist/{Year}/{Month}")]
        public async Task<IActionResult> ExisistBalance(int Year, int Month)
        {

            var accBalance = await _repo.GetAccBalance(Year, Month);

            var balanceToReturn = _mapper.Map<AccBalanceForCreateDto>(accBalance);

            if (balanceToReturn != null)
            {
                return Ok(true);
            }

            return Ok(false);

        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> AddBalanceSheet(AccBalanceForCreateDto accBalanceForCreateDto)
        {
            
            var checkExisting = await _repo.GetAccBalance(accBalanceForCreateDto.Year, accBalanceForCreateDto.Month);

            if (checkExisting != null)
            {
                _mapper.Map(accBalanceForCreateDto, checkExisting);

                await _repo.SaveAll();                   
            }
            else
            {
                var balanceSheet = _mapper.Map<AccountBalance>(accBalanceForCreateDto);

                var result = await _repo.AddAccBalance(balanceSheet);                
            }
            
            return StatusCode(201);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("report")]
        public async Task<IActionResult> GetReport([FromQuery] PeriodForReportDto periodForReportDto)
        {

            var report = await _repo.GetReport(periodForReportDto);

            if (report != null)
            {
                var reportResults = _mapper.Map<List<ReportForReturnDto>>(report);
                return Ok(reportResults);
            }


            return BadRequest("Failed to retrive report results");
        }
    }
}