using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dtos;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class AccBalanceRepository : IAccBalanceRepository
    {
        private readonly AuthenticationContext _context;
        public AccBalanceRepository(AuthenticationContext context)
        {
            _context = context;

        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public async Task<AccountBalance> AddAccBalance(AccountBalance balanceSheet)
        {
            await _context.AccountBalance.AddAsync(balanceSheet);
            await _context.SaveChangesAsync();
            return balanceSheet;
        }

        public async Task<AccountBalance> GetAccBalance(int Year, int Month)
        {
            var query = _context.AccountBalance.AsQueryable();

            var accBalance = await query.Where(b => b.Year == Year && b.Month == Month).FirstOrDefaultAsync();

            return accBalance;
        }

        public async Task<List<int>> GetYearList()
        {
            var query = _context.AccountBalance.AsQueryable();

            var yearList = await query.Select(x => x.Year).Distinct().ToListAsync();

            return yearList;
        }

        public async Task<List<int>> GetMonthList(int Year)
        {
            var query = _context.AccountBalance.AsQueryable();

            var monthList = await query.Where(b => b.Year == Year).Select(x => x.Month).Distinct().ToListAsync();

            return monthList;
        }

        public async Task<List<AccountBalance>> GetReport(PeriodForReportDto periodForReportDto)
        {

            var query = _context.AccountBalance.AsQueryable();
            var reportResults = query.OrderBy(b => b.Year).ThenBy(b => b.Month).AsQueryable();

            if (periodForReportDto.StartYear < periodForReportDto.EndYear)
            {
                reportResults = reportResults.Where(r =>
                    r.Year == periodForReportDto.StartYear && r.Month >= periodForReportDto.StartMonth
                    || r.Year > periodForReportDto.StartYear && r.Year < periodForReportDto.EndYear
                    || r.Year == periodForReportDto.EndYear && r.Month <= periodForReportDto.EndMonth);
            }

            if (periodForReportDto.StartYear == periodForReportDto.EndYear)
            {
                reportResults = reportResults.Where(r =>
                    r.Month >= periodForReportDto.StartMonth && r.Month <= periodForReportDto.EndMonth && r.Year == periodForReportDto.StartYear);
            }

            var result = await reportResults.Select(r => new AccountBalance
            {
                Year = r.Year,
                Month = r.Month,
                RAndD = r.RAndD,
                Canteen = r.Canteen,
                CeoCar = r.CeoCar,
                Marketing = r.Marketing,
                ParkingFines = r.ParkingFines                
            }).ToListAsync(); 

            return result;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }


    }
}