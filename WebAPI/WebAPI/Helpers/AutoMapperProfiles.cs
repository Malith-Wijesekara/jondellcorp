using System.Linq;
using AutoMapper;
using Dtos;
using WebAPI.Dtos;
using WebAPI.Models;

namespace WebAPI.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {            
            CreateMap<UserForRegisterDto, ApplicationUser>();
            CreateMap<AccBalanceForCreateDto, AccountBalance>();
            CreateMap<AccountBalance, AccBalanceForCreateDto>();
            CreateMap<AccountBalance, ReportForReturnDto>();            
        }
    }
}