using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos
{
    public class AccBalanceForCreateDto
    {

        [Required]  
        public int Year { get; set; }      
        [Required]  
        public int Month { get; set; }
        public double RAndD { get; set; }
        public double Canteen { get; set; }
        public double CeoCar { get; set; }
        public double Marketing { get; set; }
        public double ParkingFines { get; set; }        
    }
}