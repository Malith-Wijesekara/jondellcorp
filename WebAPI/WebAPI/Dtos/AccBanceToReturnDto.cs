using System.ComponentModel.DataAnnotations;

namespace Dtos
{
    public class AccBanceToReturnDto
    {
        [Required]  
        public int Year { get; set; }      
        [Required]  
        public int Month { get; set; }
    }
}