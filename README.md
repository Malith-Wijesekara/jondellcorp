# README #

Jondell Corp Account balance overview tool

### What is this repository? ###

A simple web enabled account balance overview tool. 

* Account balances are updated every month in the Jondell Corp.
* Updated balance can imported to the system using excel file.
* Admin users can only upload balances and view the report page.
* Admin users can add new users with roles.
* All the other users can view the current balances of each account from the system.

### Prerequisites ###

* Angular CLI: 9.0.6
* Node: 12.16.1
* Dotnet SDK 3.1.301
* SQL server

### Unit test frameworks ###

* Jasmine & Karma
* Xunit

### How to set up client app (Front-End) ###

* Run npm install in clientApp directory.
* Then run ng serve.

### How to set up WebAPI (Back-End) ###

* Run dotnet ef database update inside the WebAPI/WebAPI directory.(If dotnet ef tools not installed use this command: dotnet tool install --global dotnet-ef).
* If you have installed multiple dotnet SDKs in your machine, run this command first to create a global.json : dotnet new globaljson
* Then run dotnet run command.

### How to run unit tests ###

Front-End

* Run ng test inside the /ClientApp directory.

Back_End

* Run dotnet test inside the  WebAPI/WebAPI.Test directory

### Test user credentials ###

Admin

* Username: Test_Admin
* Password: admin

User

* Username: Test_User
* Password: user

### Upload sample file ###

* Use the given sample_balance.xlsx file for uploads.