export interface User {
  id: number;
  username: string;
  password: string;
  confirmPassword: string;
  dateOfBirth: string;
  gender: string;
  fullName: string;
  email: string;
  created: Date;
  role: string;
}
