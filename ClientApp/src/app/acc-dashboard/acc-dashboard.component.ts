import { Component, OnInit } from '@angular/core';
import { AlertifyService } from '../_services/alertify.service';
import { AccBalanceService } from '../_services/acc-balance.service';
import * as moment from 'moment';
import { MonthMap } from '../_models/MonthMap';

@Component({
  selector: 'app-acc-dashboard',
  templateUrl: './acc-dashboard.component.html'
})
export class AccDashboardComponent implements OnInit {
  filterOptions: any = {};
  public lineChartType: string = 'line';
  yearList: number[] = [];
  startMonthtList: MonthMap[] = [];
  endMonthtList?: MonthMap[] = [];

  yearMonth: string[] = [];
  rAndD: number[] = [];
  canteen: number[] = [];
  ceoCar: number[] = [];
  marketing: number[] = [];
  parkingFines: number[] = [];
  lastBalance: number[] = [];

  public lineChartDatasets: Array<any> = [
    { data: this.rAndD, label: 'R&D' },
    { data: this.canteen, label: 'Canteen' },
    { data: this.ceoCar, label: "CEO's car" },
    { data: this.marketing, label: 'Marketing' },
    { data: this.parkingFines, label: 'Parking fines' },
  ];

  public lineChartLabels: Array<any> = [];

  public lineChartColors: Array<any> = [
    {
      borderColor: 'rgba(153, 102, 0)',
      borderWidth: 1,
    },
    {
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 1,
    },
    {
      borderColor: 'rgba(0, 102, 0)',
      borderWidth: 1,
    },
    {
      borderColor: 'rgba(255, 0, 0)',
      borderWidth: 1,
    },
    {
      borderColor: 'rgba(204, 51, 255)',
      borderWidth: 1,
    },
  ];

  public chartOptions: any = {
    responsive: true,
  };

  public barChartType: string = 'bar';

  public barChartDatasets: Array<any> = [
    { data: [], label: 'Balance at the end of the period' },
  ];

  public barChartLabels: Array<any> = [
    'R&D',
    'Canteen',
    "CEO's car",
    'Marketing',
    'Parking fines',
  ];

  public barChartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      borderWidth: 1,
    },
  ];

  public chartClicked(e: any): void {}
  public chartHovered(e: any): void {}

  constructor(
    private alertifyService: AlertifyService,
    private balanceService: AccBalanceService
  ) {}

  ngOnInit(): void {
    this.getYearList();
  }

  getYearList(): void {
    this.balanceService.getYearList().subscribe(
      (data: any) => {
        if (data) {
          this.yearList = data;
        }
      },
      (error) => {
        this.alertifyService.error(error.error);
      }
    );
  }

  getStartMonthList(year): void {
    this.balanceService.getMonthList(year).subscribe(
      (data: any) => {
        if (data) {
          this.filterOptions.startMonth = null;
          this.startMonthtList = this.balanceService.mapMonthId(data);
        }
      },
      (error) => {
        this.alertifyService.error(error.error);
      }
    );
  }

  getEndtMonthList(year): void {
    this.balanceService.getMonthList(year).subscribe(
      (data: any) => {
        if (data) {
          this.filterOptions.endMonth = null;
          this.endMonthtList = this.balanceService.mapMonthId(data);
        }
      },
      (error) => {
        this.alertifyService.error(error.error);
      }
    );
  }

  getReport(): void {
    if (
      this.filterOptions.startYear === null ||
      this.filterOptions.startMonth === null ||
      this.filterOptions.endYear === null ||
      this.filterOptions.endMonth === null
    ) {
      this.alertifyService.warning(
        'Please select year and month for both ends'
      );
      return;
    }

    const startDate = moment(
      this.filterOptions.startYear + '/' + this.filterOptions.startMonth + '/01'
    );
    const EndDate = moment(
      this.filterOptions.endYear + '/' + this.filterOptions.endMonth + '/30'
    );

    if (EndDate.diff(startDate, 'days') <= 0) {
      this.alertifyService.warning('End date should be grater that Start date');
      return;
    }

    this.balanceService.getReport(this.filterOptions).subscribe(
      (data) => {
        if (data) {
          this.generateLineChart(data);
        }
      },
      (error) => {
        this.alertifyService.error(error.statusText);
      }
    );
  }

  generateLineChart(reportData): void {
    this.rAndD.length = 0;
    this.canteen.length = 0;
    this.ceoCar.length = 0;
    this.marketing.length = 0;
    this.parkingFines.length = 0;
    this.lineChartLabels.length = 0;

    reportData.forEach((balance) => {
      this.lineChartLabels.push(balance.year + '/' + balance.month);
      if (this.filterOptions.randD) {
        this.rAndD.push(balance.rAndD);
      }
      if (this.filterOptions.canteen) {
        this.canteen.push(balance.canteen);
      }
      if (this.filterOptions.ceoCar) {
        this.ceoCar.push(balance.ceoCar);
      }
      if (this.filterOptions.marketing) {
        this.marketing.push(balance.marketing);
      }
      if (this.filterOptions.parkingFines) {
        this.parkingFines.push(balance.parkingFines);
      }
    });

    this.generateBarChart(reportData);
  }

  generateBarChart(reportData): void {
    this.lastBalance.length = 0;

    let lastBalance = reportData[reportData.length - 1];
    this.lastBalance.push(lastBalance.rAndD);
    this.lastBalance.push(lastBalance.canteen);
    this.lastBalance.push(lastBalance.ceoCar);
    this.lastBalance.push(lastBalance.marketing);
    this.lastBalance.push(lastBalance.parkingFines);

    this.barChartDatasets = [
      { data: this.lastBalance, label: 'Balance at the end of the period' },
    ];
  }
}
