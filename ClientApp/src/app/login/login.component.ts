import { Component, OnInit } from '@angular/core';
import { AlertifyService } from './../_services/alertify.service';
import { AuthService } from './../_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [`
    .form-group{
    margin-top: 10%;
   }
   .card{
       margin-top: 40%;
   }
  `],
})

export class LoginComponent implements OnInit {
  model: any = {};
  constructor(
    public authService: AuthService,
    private alertifyService: AlertifyService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  login(): void {
    this.authService.login(this.model).subscribe(
      (next) => {
        this.alertifyService.success('Logged in successfully');
      },
      (error) => {
        this.alertifyService.error(error.statusText);
      },
      () => {
        if (localStorage.getItem('role') === 'Admin') {
          this.router.navigate(['/dashboard']);
        } else {
          this.router.navigate(['/balance']);
        }
      }
    );
  }
}
