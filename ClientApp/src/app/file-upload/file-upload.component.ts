import { AccBalanceService } from './../_services/acc-balance.service';
import { Balance } from './../_models/Balance';
import { environment } from './../../environments/environment';
import { Component, OnInit } from '@angular/core';

import * as XLSX from 'xlsx';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html'
})
export class FileUploadComponent implements OnInit {
  baseUrl = environment.apiUrl;
  accountPeriod: any = {};
  balanceSheet: any = {};
  isValidFile = false;
  fileTypes = [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
  ];

  constructor(
    private alertifyService: AlertifyService,
    private balanceService: AccBalanceService
  ) {}

  ngOnInit(): void {}

  onFileChange(event: any): void {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    if (this.checkFileType(file) === true) {
      reader.onload = (event) => {
        const data = reader.result;
        workBook = XLSX.read(data, { type: 'binary' });
        jsonData = workBook.SheetNames.reduce((initial, name) => {
          const sheet = workBook.Sheets[name];
          initial[name] = XLSX.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        console.log(jsonData);
        const importData = jsonData.Sheet1;
        this.isValidFile = importData;
        this.balanceSheet.rAndD = importData[0].__EMPTY;
        this.balanceSheet.canteen = importData[1].__EMPTY;
        this.balanceSheet.ceoCar = importData[2].__EMPTY;
        this.balanceSheet.marketing = importData[3].__EMPTY;
        this.balanceSheet.parkingFines = importData[4].__EMPTY;
        console.log(this.balanceSheet);
      };
      reader.readAsBinaryString(file);
    }
  }

  createOrUpdateRecord(): void {
    this.balanceService.saveFile(this.balanceSheet).subscribe(
      (next) => {
        this.alertifyService.success('File uploaded successfully');
      },
      (error) => {
        this.alertifyService.error('Upload failed!');
      }
    );
  }

  saveFile(): void {
    this.balanceSheet.year = this.accountPeriod.year;
    this.balanceSheet.month = this.accountPeriod.month;

    if (
      this.balanceSheet.year === undefined &&
      this.balanceSheet.month === undefined
    ) {
      this.alertifyService.error('Please select year and month for the file');
    }
    this.balanceService
      .checkExistdBalance(this.accountPeriod)
      .subscribe((data) => {
        if (data === true) {
          if (
            confirm(
              'A record is exists for the given account period. Do you want to override it?'
            )
          ) {
            this.createOrUpdateRecord();
          }
        } else {
          this.createOrUpdateRecord();
        }
      });
  }

  checkFileType(file): boolean {
    if (!this.fileTypes.includes(file.type)) {
      this.alertifyService.error(
        'File type not allowed! Please select either .xls or .xlsx file'
      );
      return false;
    } else {
      return true;
    }
  }
}
