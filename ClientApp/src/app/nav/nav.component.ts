import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styles: [
    `
      .dropdown {
        cursor: pointer;
      }
    `,
  ],
})
export class NavComponent implements OnInit {
  jwtHelper = new JwtHelperService();
  constructor(
    public authService: AuthService,
    private alertifyService: AlertifyService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  loggedIn(): boolean {
    return this.authService.loggedIn();
  }

  getUsername(): string {
    if (this.loggedIn) {
      const decodedToken = this.jwtHelper.decodeToken(
        localStorage.getItem('token')
      );
      return decodedToken.unique_name;
    }
  }

  logout(): void {
    localStorage.removeItem('token');
    this.alertifyService.message('Logged out');
    this.router.navigate(['/login']);
  }
}
